## 1 установить docker for windows + linux
Перед запуском Ubuntu 18.04 необходимо включить Windows WSL и Windows Virtual Machine Platform посредством выполнения двух команд в PowerShell:
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux (потребует перезагрузку компьютера)
Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform
После необходимо удостовериться, что мы будем использовать WSL v2. Для этого в терминале WSL или PowerShell последовательно выполняем команды:
wsl -l -v — смотрим, какая версия установлена в данный момент. Если 1, то движемся далее по списку
wsl --set-version ubuntu 18.04 2 — для обновления до версии 2
wsl -s ubuntu 18.04 — устанавливаем Ubuntu 18.04 в качестве дистрибутива по-умолчанию
Теперь можно запустить Ubuntu 18.04, провести настройку (указать имя пользователя и пароль).

## обновить linux 

## установить git  sudo apt-get install git
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com

## создать папку .ssh внутри linux 
положить id_rsa, config

## информация о distro linux
cat /etc/issue
cat /etc/debian_version
cat /etc/os-release

## Установить fish 
Для Debian 10 запустите:
Keep in mind that the owner of the key may distribute updates, packages and repositories that your system will trust (more information).

echo 'deb http://download.opensuse.org/repositories/shells:/fish:/release:/3/Debian_10/ /' | sudo tee /etc/apt/sources.list.d/shells:fish:release:3.list
curl -fsSL https://download.opensuse.org/repositories/shells:fish:release:3/Debian_10/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/shells_fish_release_3.gpg > /dev/null
sudo apt update
sudo apt install fish

## установить fish как shell по умолчанию
sudo usermod -s /usr/bin/fish username
Изменять цвета в 
~/.config/fish/fish_variables

## установить fisher
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
## установить fnm (nvm) 
fisher install jorgebucaran/nvm.fish

## Работа с проектом через PhpStorm
Чтобы обеспечить возможность работы с проектом в WSL через PhpStorm, необходимо сделать следующее:
Запускаем командную строку Windows 10 от имени администратора;
В командной строке Windows 10 выполняем команду mklink /D C:\project_directory \\wsl$\Ubuntu\home\user\project_directory, которая создаст символьную ссылку на папку проекта.
После этого в IDE можно открыть проект по пути C:\project_directory

# mysql network
docker port docker-sever_db_1 3306
docker container inspect docker-server_db_1